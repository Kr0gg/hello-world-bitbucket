﻿using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Xerris.DotNet.Core.Aws.Api;

namespace Hello.Lambda.Handlers
{
    public class BlogHandler 
    {
        [LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]
        public APIGatewayProxyResponse SayHello(APIGatewayProxyRequest request)
        {
            return "Hello from my Lambda".Ok();
        }
    }
}