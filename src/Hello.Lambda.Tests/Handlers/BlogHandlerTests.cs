using Amazon.Lambda.APIGatewayEvents;
using FluentAssertions;
using Hello.Lambda.Handlers;
using Xunit;

namespace Hello.Lambda.Tests.Handlers
{
    public class BlogHandlerTests
    {
        private readonly BlogHandler handler;

        public BlogHandlerTests()
        {
            handler = new BlogHandler();
        }

        [Fact]
        public void SayHello()
        {
            var response = handler.SayHello(new APIGatewayProxyRequest());
            var helloMessage = response.Body;
            helloMessage.Should().Be("Hello from my Lambda");
        }
    }
}